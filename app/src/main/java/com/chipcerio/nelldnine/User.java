package com.chipcerio.nelldnine;

import com.google.gson.annotations.Expose;

public class User {

    @Expose
    private String created;

    @Expose
    private String email;

    @Expose
    private String name;

    @Expose
    private String updated;

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }
}