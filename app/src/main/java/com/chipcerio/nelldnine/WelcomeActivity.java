package com.chipcerio.nelldnine;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class WelcomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        String email = getIntent().getStringExtra(MainActivity.EXTRA_EMAIL);

        TextView textView = (TextView) findViewById(R.id.txt_welcome_email);
        textView.setText("Halur " + email + "!");
    }
}
