package com.chipcerio.nelldnine;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;


/*
 * login
 * validate
 * another screen
 */
public class MainActivity extends AppCompatActivity {

    public static final String EXTRA_EMAIL = "nelldnine:email";

    private ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // initialise
        final EditText email = (EditText) findViewById(R.id.txt_email);
        final EditText password = (EditText) findViewById(R.id.txt_password);
        Button login = (Button) findViewById(R.id.button);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // get email and password
                String emailStr = email.getText().toString();
                String passwdStr = password.getText().toString();

                dialog = ProgressDialog.show(MainActivity.this, "", "Logging in...", true, false);
                // http post request
                // http://symph-login-api.appspot.com/
                http(emailStr, passwdStr);
            }
        });
    }

    private void http(String em, String pw) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://symph-login-api.appspot.com")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        LoginService loginService = retrofit.create(LoginService.class);
        Call<User> call = loginService.login(em, pw);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (response.isSuccessful()) {
                    User user = response.body();
                    String email = user.getEmail();
                    Intent welcomeIntent = new Intent(MainActivity.this, WelcomeActivity.class);
                    welcomeIntent.putExtra(EXTRA_EMAIL, email);
                    startActivity(welcomeIntent);
                    dialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
            }
        });
    }

    interface LoginService {
        @FormUrlEncoded
        @POST("/login")
        Call<User> login(
                @Field("email") String em,
                @Field("password") String pw
        );
    }


}
